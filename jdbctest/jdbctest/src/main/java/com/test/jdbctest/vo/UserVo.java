package com.test.jdbctest.vo;

import lombok.Data;
/**
 * VO Obj used to reieve Userobj input
 * 
 * @author moham
 *
 */
@Data
public class UserVo{

	private String username;
	private String address;
	private String emailId;
	private String txId;

}
