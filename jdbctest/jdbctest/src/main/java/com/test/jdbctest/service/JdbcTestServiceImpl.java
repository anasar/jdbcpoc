package com.test.jdbctest.service;

import org.springframework.stereotype.Service;

import com.test.jdbctest.dao.JdbcTestDao;
import com.test.jdbctest.vo.ResponseMsgVo;
import com.test.jdbctest.vo.UserVo;

@Service
public class JdbcTestServiceImpl implements JdbcTestService {

	private JdbcTestDao jbdcDao;

	public JdbcTestServiceImpl(JdbcTestDao jbdcDao) {
		this.jbdcDao = jbdcDao;
	}

	@Override
	public ResponseMsgVo updateUser(UserVo user) {

		ResponseMsgVo responseMsg = new ResponseMsgVo();

		if (jbdcDao.isUserExsist(user.getUsername())) {

			if (jbdcDao.updateUserObj(user)) {
				responseMsg.setMsg("Successfully Updated");
			} else {
				responseMsg.setMsg("Update failed !!!");
			}
		}
		else
			responseMsg.setMsg("Update failed !!! User doesn't exsist !!!");
		
		return responseMsg;
	}

}
