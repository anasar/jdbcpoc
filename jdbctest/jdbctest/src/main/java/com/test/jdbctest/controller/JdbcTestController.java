package com.test.jdbctest.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.jdbctest.service.JdbcTestService;
import com.test.jdbctest.vo.ResponseMsgVo;
import com.test.jdbctest.vo.UserVo;

@RestController
public class JdbcTestController {

	private JdbcTestService jdbcTestService;

	public JdbcTestController(JdbcTestService jdbcTestService) {
		this.jdbcTestService = jdbcTestService;
	}

	/**
	 * 
	 * @param user
	 * @return
	 * 
	 * 
	 * UPDATING ONLY FOUR COLUMN OF 'TBFO_TEST_USER' [ TABLE HAVE TOTAL : 20 COLUMNS ] 
	 * 
	 */
	@RequestMapping("/updateUser")
	public ResponseMsgVo updateUser(@RequestBody UserVo user) {

		return jdbcTestService.updateUser(user);
	}

}
