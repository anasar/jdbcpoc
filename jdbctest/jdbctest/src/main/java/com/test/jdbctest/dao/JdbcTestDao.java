
package com.test.jdbctest.dao;

import com.test.jdbctest.vo.UserVo;

public interface JdbcTestDao {

	public boolean updateUserObj(UserVo user);

	public boolean isUserExsist(String username);
}
