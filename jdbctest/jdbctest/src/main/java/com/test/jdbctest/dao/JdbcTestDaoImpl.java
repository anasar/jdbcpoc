package com.test.jdbctest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.jdbctest.vo.UserVo;

@Component
public class JdbcTestDaoImpl implements JdbcTestDao {

	@Autowired
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public boolean updateUserObj(UserVo user) {

		String query = "Update tbf0_test_user set address = ? , email= ? , tax_id = ? where user_name = ?";
		try (Connection con = dataSource.getConnection(); PreparedStatement ps = con.prepareStatement(query)) {

			ps.setString(1, user.getAddress());
			ps.setString(2, user.getEmailId());
			ps.setString(3, user.getTxId());
			ps.setString(4, user.getUsername());

			int out = ps.executeUpdate();
			if (out != 0) {
				System.out.println("User updated with " + user.getUsername());
				return true;
			} else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean isUserExsist(String username) {

		String query = "select user_id from tbf0_test_user where user_name = ?";
		try (Connection con = dataSource.getConnection(); PreparedStatement ps = con.prepareStatement(query)) {

			ps.setString(1, username);
			return ps.executeQuery().next();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
