package com.test.jdbctest.vo;

import lombok.Data;

/**
 * Response Obj for API
 * @author moham
 *
 */
@Data
public class ResponseMsgVo {
	
	private String msg;
	private String status;

}
