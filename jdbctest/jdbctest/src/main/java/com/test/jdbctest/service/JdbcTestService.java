package com.test.jdbctest.service;

import com.test.jdbctest.vo.ResponseMsgVo;
import com.test.jdbctest.vo.UserVo;

public interface JdbcTestService {

	public ResponseMsgVo updateUser(UserVo user);
}
